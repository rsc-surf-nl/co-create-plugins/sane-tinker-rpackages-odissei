$LOGFILE = "c:\logs\SANE-tinker-rpackages-Odissei.log"

Function Write-Log([String] $logText) {
  '{0:u}: {1}' -f (Get-Date), $logText | Out-File $LOGFILE -Append
}

Function Main {
     Write-Log "Start installing SANE-tinker-rpackages-Odissei"

    #  foreach($line in Get-Content "$PSScriptRoot\packages.txt") {
    #    $script = [string]::Concat("install.packages('",$line.trim(),"', repos='https://cran.rstudio.com/'", ", quiet = TRUE)")
    #    RScript.exe  -e $script >> $LOGFILE
    # }

    Write-Log "End installing SANE-tinker-rpackages-Odissei"
}

Main  
